# BYU Standard Change SDK
[![coverage report](https://gitlab.com/tom.davidson/byu-standard-change-sdk/badges/master/coverage.svg)](https://gitlab.com/tom.davidson/byu-standard-change-sdk/commits/master) [![build status](https://gitlab.com/tom.davidson/byu-standard-change-sdk/badges/master/build.svg)](https://gitlab.com/tom.davidson/byu-standard-change-sdk/commits/master)

Create Standard RFCs through the [Standard Change web service](https://api.byu.edu/store/apis/info?name=StandardChange&version=v1&provider=BYU/dlb44) with:

- JavaScript module - fully tree-shakable for consumers using es6 imports (like Rollup or Webpack 2)
- CommonJS module for backwards compatibility with Node.js-style imports
- Type declarations to improve your downstream development experience
- Auto-generated API documentation with TypeDoc
- Based on Standard Change web service's Swagger

Project Tech includes:

- TypeScript - with stable ES7 features - for  tooling, linting, and documentation
- Collocated, atomic, concurrent unit tests with Mocah/Chai
- Source-mapped code coverage reports with nyc
- Code coverage testing (for continuous integration)

## Installation

```bash
$ npm i -S byu-standard-change-sdk
```
