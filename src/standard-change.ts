/**
 * StandardChange
 * Create Standard Changes through web service.
 *
 */

import * as request from 'request';
import * as http from 'http';

const defaultBasePath = 'https://api.byu.edu:443/domains/servicenow/standardchange/v1';

/* tslint:disable:no-unused-variable */

export class Body {
  /**
  * Multiple standard change requests may be submitted
  */
  changes: ChangeRequestChanges[];
}

export class ChangeRequestChanges {
  /**
  * The Net ID of the user responsible for the Change Request default to the logged in user
  */
  assignedTo: string;
  /**
  * This is the short description of the Change Request
  */
  shortDescription: string;
  /**
  * This is the description field on the Change Request
  */
  description: string;
  /**
  * YYYY-MM-DD is a valid format
  */
  requestDate: Date;
  /**
  * 10-Draft 20-Submitted, defaults to Submitted which is automatically approved
  */
  state: ChangeRequestChanges.StateEnum;
  /**
  * The sysId of the standard change template
  */
  templateId: string;
}

export namespace ChangeRequestChanges {
  export enum StateEnum {
    _10 = <any>'10',
    _20 = <any>'20',
  }
}
/**
*
*/
export class InlineResponse200 {
  result: InlineResponse200Result[];
}

export class InlineResponse200Result {
  shortDescription: string;
  status: string;
  changeSysId: string;
  number: string;
}


export interface Authentication {
  /**
  * Apply authentication settings to header and query params.
  */
  applyToRequest(requestOptions: request.Options): void;
}

export class HttpBasicAuth implements Authentication {
  public username: string;
  public password: string;
  applyToRequest(requestOptions: request.Options): void {
    requestOptions.auth = { username: this.username, password: this.password };
  }
}

export class ApiKeyAuth implements Authentication {
  public apiKey: string;

  constructor(private location: string, private paramName: string) {
  }

  applyToRequest(requestOptions: request.Options): void {
    if (this.location === 'query') {
      (<any>requestOptions.qs)[this.paramName] = this.apiKey;
    } else if (this.location === 'header' && requestOptions && requestOptions.headers) {
      requestOptions.headers[this.paramName] = this.apiKey;
    }
  }
}

export class OAuth implements Authentication {
  public accessToken: string;

  applyToRequest(requestOptions: request.Options): void {
    if (requestOptions && requestOptions.headers) {
      requestOptions.headers['Authorization'] = 'Bearer ' + this.accessToken;
    }
  }
}

export class VoidAuth implements Authentication {
  public username: string;
  public password: string;
  applyToRequest(_: request.Options): void {
    // Do nothing
  }
}

export enum DefaultApiApiKeys {
}

export class DefaultApi {
  protected basePath = defaultBasePath;
  protected defaultHeaders: any = {};
  protected useQueryString: boolean = false;

  protected authentications = { default: <Authentication>new VoidAuth() };

  constructor(basePath?: string);
  constructor(basePathOrUsername: string, password?: string, basePath?: string) {
    if (password) {
      if (basePath) {
        this.basePath = basePath;
      }
    } else {
      if (basePathOrUsername) {
        this.basePath = basePathOrUsername;
      }
    }
  }

  set useQuerystring(value: boolean) {
    this.useQueryString = value;
  }

  public setApiKey(key: DefaultApiApiKeys, value: string) {
    // tslint:disable-next-line
    this.authentications[DefaultApiApiKeys[key]].apiKey = value;
  }
  /**
   * Create Standard Changes
   *
   * @param body
   */
  public createStandardChangeRequest(body: Body):
    Promise<{ response: http.ClientResponse; body: InlineResponse200; }> {
    const localVarPath = this.basePath + '/change_request';
    const queryParameters: any = {};
    const headerParams: any = (<any>Object).assign({}, this.defaultHeaders);
    const formParams: any = {};


    // verify required parameter 'body' is not null or undefined
    if (body === null || body === undefined) {
      throw new Error(`Required parameter body was null or undefined when
        calling createStandardChangeRequest.`);
    }

    const useFormData = false;

    const requestOptions: request.Options = {
      method: 'PUT',
      qs: queryParameters,
      headers: headerParams,
      uri: localVarPath,
      useQuerystring: this.useQueryString,
      json: true,
      body,
    };

    this.authentications.default.applyToRequest(requestOptions);

    if (Object.keys(formParams).length) {
      if (useFormData) {
        (<any>requestOptions).formData = formParams;
      } else {
        requestOptions.form = formParams;
      }
    }
    return new Promise<{
      response: http.ClientResponse; body: InlineResponse200;
    }>((resolve, reject) => {
      // tslint:disable-next-line
      request(requestOptions, (error, response, body) => {
        if (error) {
          reject(error);
        } else {
          if (response.statusCode && response.statusCode >= 200 && response.statusCode <= 299) {
            resolve({ response, body });
          } else {
            reject({ response, body });
          }
        }
      });
    });
  }
}
